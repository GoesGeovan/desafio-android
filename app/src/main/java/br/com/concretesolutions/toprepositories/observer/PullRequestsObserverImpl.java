package br.com.concretesolutions.toprepositories.observer;

import android.util.Log;

import br.com.concretesolutions.toprepositories.callback.PullRequestsCallback;
import br.com.concretesolutions.toprepositories.model.Repositorio;
import br.com.concretesolutions.toprepositories.service.GitHubService;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by geovan
 */

public class PullRequestsObserverImpl {

    private PullRequestsCallback pullRequestsCallback;

    private GitHubService gitHubService;

    public PullRequestsObserverImpl(PullRequestsCallback pullRequestsCallback, GitHubService gitHubService) {
        this.pullRequestsCallback = pullRequestsCallback;
        this.gitHubService = gitHubService;
    }

    public void obterPullRequests(Repositorio repositorio) {
        String repoName = repositorio.getName();
        String loginUserRepo = repositorio.getUser().getLogin();

        gitHubService.obterPullRequests(loginUserRepo, repoName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(pullRequests -> {
                    pullRequestsCallback.setarQuantidadeCallbacks(pullRequests.size());
                    return Observable.fromIterable(pullRequests);
                })
                .flatMap(pullRequest -> {
                    String loginUserPull = pullRequest.getUser().getLogin();
                    gitHubService
                            .obterDadosDoUsuarioo(loginUserPull)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(owner -> {
                                pullRequest.setUser(owner);
                                pullRequestsCallback.inserirNaLista(pullRequest);
                            }, throwable -> {
                                Log.d("PullReqObserverOwner", throwable.getMessage());
                                pullRequestsCallback.inserirNaLista(pullRequest);
                            }, () -> {
                            });
                    return Observable.just(pullRequest);
                })
                .subscribe(pullRequest -> {
                }, throwable -> {
                    Log.d("PullReqObserver", throwable.getMessage());
                    pullRequestsCallback.tratarErro(throwable);
                });
    }

}
