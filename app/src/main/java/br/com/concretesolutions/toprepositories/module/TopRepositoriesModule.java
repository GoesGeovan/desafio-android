package br.com.concretesolutions.toprepositories.module;

import android.app.Application;

import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import br.com.concretesolutions.toprepositories.service.GitHubService;
import dagger.Module;
import dagger.Provides;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by geovan.goes
 */
@Module
public class TopRepositoriesModule {
    private Application application;

    public TopRepositoriesModule(Application application) {
        this.application = application;
    }


    @Provides
    public GitHubService getGitHubService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder();

        /**
         * Colocar credenciais para evitar limite baixo de chamadas a API
         * */
        String authToken = Credentials.basic("toprepositories", "abc!123");

        client.addInterceptor(chain -> {
            Response response = chain.proceed(chain.request());
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header("Authorization", authToken)
                    .build();
            response = chain.proceed(request);
            return response;
        });

        client.connectTimeout(2, TimeUnit.MINUTES);
        client.readTimeout(2, TimeUnit.MINUTES);

        Retrofit retrofit;

        retrofit = new Retrofit
                .Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client.build())
                .build();

        return retrofit.create(GitHubService.class);
    }

    @Provides
    public Picasso getPicasso() {
        Picasso picasso = new Picasso.Builder(application).build();
        picasso.setIndicatorsEnabled(true);
        return picasso;
    }
}
