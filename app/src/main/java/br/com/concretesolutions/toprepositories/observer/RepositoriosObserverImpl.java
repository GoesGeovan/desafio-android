package br.com.concretesolutions.toprepositories.observer;

import android.util.Log;

import br.com.concretesolutions.toprepositories.callback.RepositoriosCallback;
import br.com.concretesolutions.toprepositories.service.GitHubService;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by geovan
 */

public class RepositoriosObserverImpl {

    private RepositoriosCallback repositoriosCallback;

    private GitHubService gitHubService;

    public RepositoriosObserverImpl(RepositoriosCallback repositoriosCallback, GitHubService gitHubService) {
        this.repositoriosCallback = repositoriosCallback;
        this.gitHubService = gitHubService;
    }

    public void obterRepositorios(int numeroPagina) {
        gitHubService
                .obterRepositoriosObs(String.valueOf(numeroPagina))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(repositoriosTO -> {
                    repositoriosCallback.setarQuantidadeCallbacks(repositoriosTO.getRespositorios().size());
                    return Observable.fromIterable(repositoriosTO.getRespositorios());
                })
                .flatMap(repositorio -> {
                    String login = repositorio.getUser().getLogin();
                    gitHubService
                            .obterDadosDoUsuarioo(login)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(owner -> {
                                repositorio.setUser(owner);
                                repositoriosCallback.inserirNaLista(repositorio);
                            }, throwable -> {
                                repositoriosCallback.inserirNaLista(repositorio);
                            }, () -> {
                            });
                    return Observable.just(repositorio);
                })
                .subscribe(repositorio -> {
                }, throwable -> {
                    Log.d("RepositoriosObserver", throwable.getMessage());
                    repositoriosCallback.tratarErro(throwable);
                });
    }

}
