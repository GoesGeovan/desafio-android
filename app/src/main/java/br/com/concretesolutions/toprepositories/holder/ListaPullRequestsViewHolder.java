package br.com.concretesolutions.toprepositories.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.concretesolutions.toprepositories.R;
import br.com.concretesolutions.toprepositories.listener.ItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by geovan
 */

public class ListaPullRequestsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.titulo_pull_request)
    public TextView tituloPullRequest;

    @BindView(R.id.data_pull_request)
    public TextView dataPullRequest;

    @BindView(R.id.descricao_pull_request)
    public TextView descricaoPullRequest;

    @BindView(R.id.avatar_usuario)
    public ImageView avatarUsuario;

    @BindView(R.id.username)
    public TextView username;

    @BindView(R.id.nome_sobrenome)
    public TextView nomeSobrenome;

    private ItemClickListener onClickListener;

    public ListaPullRequestsViewHolder(View itemDaLista)
    {
        super(itemDaLista);
        itemDaLista.setOnClickListener(this);
        ButterKnife.bind(this, itemDaLista);
    }

    public void setOnClickListener(ItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    @Override
    public void onClick(View view)
    {
        if (onClickListener != null)
            onClickListener.onItemClick(getAdapterPosition());
    }
}
