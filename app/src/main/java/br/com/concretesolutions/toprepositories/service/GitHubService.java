package br.com.concretesolutions.toprepositories.service;

import java.util.List;

import br.com.concretesolutions.toprepositories.model.PullRequest;
import br.com.concretesolutions.toprepositories.model.User;
import br.com.concretesolutions.toprepositories.to.RepositoriosTO;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by geovan.goes
 */

public interface GitHubService {
    /***
     * Reponsável por obter os repositórios de uma página
     * @param pagina
     * @return
     */
    @GET("search/repositories?q=language:Java&sort=stars&")
    Observable<RepositoriosTO> obterRepositoriosObs(@Query("page") String pagina);

    /***
     * Reponsável por obter os dados do usuário (Nome)
     * @param login
     * @return
     */
    @GET("users/{login}")
    Observable<User> obterDadosDoUsuarioo (@Path("login") String login);

    /***
     *
     * @param login
     * @param repoName
     * @return
     */
    @GET("repos/{login}/{repositorieName}/pulls")
    Observable<List<PullRequest>> obterPullRequests(@Path("login") String login, @Path("repositorieName") String repoName);
}
