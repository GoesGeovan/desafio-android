package br.com.concretesolutions.toprepositories.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.concretesolutions.toprepositories.R;
import br.com.concretesolutions.toprepositories.adapter.ListaPullRequestsAdapter;
import br.com.concretesolutions.toprepositories.app.TopRepositoriesApplication;
import br.com.concretesolutions.toprepositories.callback.PullRequestsCallback;
import br.com.concretesolutions.toprepositories.component.TopRepositoriesComponent;
import br.com.concretesolutions.toprepositories.model.PullRequest;
import br.com.concretesolutions.toprepositories.model.Repositorio;
import br.com.concretesolutions.toprepositories.observer.PullRequestsObserverImpl;
import br.com.concretesolutions.toprepositories.service.GitHubService;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PullRequestsActivity extends AppCompatActivity implements PullRequestsCallback {

    @BindView(R.id.recycler_lista_pull_requests)
    RecyclerView recyclerViewPullRequests;

    @Inject
    GitHubService gitHubService;

    @BindView(R.id.progress_loader)
    ProgressBar progressBar;

    private TopRepositoriesComponent topRepositoriesComponent;

    private List<PullRequest> pullRequests;

    int quantidadeCallbacks = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);

        TopRepositoriesApplication application = (TopRepositoriesApplication) getApplication();
        topRepositoriesComponent = application.getTopRepositoriesComponent();
        topRepositoriesComponent.inject(this);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        Repositorio repositorio = (Repositorio) intent.getSerializableExtra("repositorio");

        setTitle(repositorio.getName());

        pullRequests = new ArrayList<>();

        obterPullRequests(repositorio);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewPullRequests.setLayoutManager(linearLayoutManager);

        ListaPullRequestsAdapter adapter = new ListaPullRequestsAdapter(this, new ArrayList<>());

        adapter.setOnItemClickListener(position -> abrirNoBrowser(position));

        recyclerViewPullRequests.setAdapter(adapter);
    }

    private void setTitle(String repoName) {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null && repoName != null) {
            actionBar.setTitle(repoName);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void obterPullRequests(Repositorio repositorio) {
        progressBar.setVisibility(View.VISIBLE);
        new PullRequestsObserverImpl(this, gitHubService).obterPullRequests(repositorio);
    }

    private synchronized void inserirNaListaDePullRequest(PullRequest pullRequest) {
        pullRequests.add(pullRequest);
        if (quantidadeCallbacks == pullRequests.size())
        {
            inserirNaLista(pullRequests);
            quantidadeCallbacks = 0;
            pullRequests.clear();
            progressBar.setVisibility(View.GONE);
        }
    }

    private void inserirNaLista(List<PullRequest> pullRequests) {
        ListaPullRequestsAdapter adapter = (ListaPullRequestsAdapter) recyclerViewPullRequests.getAdapter();
        adapter.addItems(pullRequests);
    }

    private void abrirNoBrowser(int position) {
        ListaPullRequestsAdapter adapter = (ListaPullRequestsAdapter) recyclerViewPullRequests.getAdapter();
        PullRequest pullRequest = adapter.geItem(position);
        Intent intentAbrirBrowser = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(pullRequest.getHtmlUrl()));
        startActivity(intentAbrirBrowser);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void inserirNaLista(PullRequest pullRequest) {
        inserirNaListaDePullRequest(pullRequest);
    }

    @Override
    public void setarQuantidadeCallbacks(int quantidadeCallbacks) {
        this.quantidadeCallbacks = quantidadeCallbacks;
    }

    @Override
    public void tratarErro(Throwable throwable) {
        progressBar.setVisibility(View.GONE);
        Snackbar.make(this.recyclerViewPullRequests, "Não foi possível obter a lista de Pull Requests.", Snackbar.LENGTH_LONG).show();
    }
}
