package br.com.concretesolutions.toprepositories.callback;

import br.com.concretesolutions.toprepositories.model.PullRequest;

/**
 * Created by geovan
 */

public interface PullRequestsCallback {
    void inserirNaLista(PullRequest pullRequest);
    void setarQuantidadeCallbacks(int quantidadeCallbacks);
    void tratarErro(Throwable throwable);
}
