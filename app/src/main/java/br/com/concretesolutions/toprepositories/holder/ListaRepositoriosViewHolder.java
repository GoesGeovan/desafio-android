package br.com.concretesolutions.toprepositories.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.concretesolutions.toprepositories.R;
import br.com.concretesolutions.toprepositories.listener.ItemClickListener;
import br.com.concretesolutions.toprepositories.model.Repositorio;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by geovan
 */

public class ListaRepositoriosViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.nome_repositorio)
    public TextView nomeRepositorio;

    @BindView(R.id.descricao_repositorio)
    public TextView descricao;

    @BindView(R.id.quantidade_forks)
    public TextView quantidadeForks;

    @BindView(R.id.quantidade_stars)
    public TextView quantidadeStars;

    @BindView(R.id.username)
    public TextView username;

    @BindView(R.id.nome_sobrenome)
    public TextView nomeSobrenome;

    @BindView(R.id.avatar_usuario)
    public ImageView avatarUsuario;

    private Repositorio repositorio;
    private ItemClickListener onClickListener;

    /***
     *
     * @param itemView
     */
    public ListaRepositoriosViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        ButterKnife.bind(this, itemView);
    }

    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    public void setOnClickListener(ItemClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public void onClick(View view) {
        if (onClickListener != null)
            onClickListener.onItemClick(getAdapterPosition());
    }
}
