package br.com.concretesolutions.toprepositories.activity;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.concretesolutions.toprepositories.R;
import br.com.concretesolutions.toprepositories.adapter.ListaRepositoriosAdapter;
import br.com.concretesolutions.toprepositories.app.TopRepositoriesApplication;
import br.com.concretesolutions.toprepositories.callback.RepositoriosCallback;
import br.com.concretesolutions.toprepositories.component.TopRepositoriesComponent;
import br.com.concretesolutions.toprepositories.listener.EndlessRecyclerViewScrollListener;
import br.com.concretesolutions.toprepositories.model.Repositorio;
import br.com.concretesolutions.toprepositories.observer.RepositoriosObserverImpl;
import br.com.concretesolutions.toprepositories.service.GitHubService;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements RepositoriosCallback {

    @Inject
    GitHubService gitHubService;

    @Inject
    Picasso picasso;

    @BindView(R.id.recycler_lista)
    RecyclerView recyclerView;

    @BindView(R.id.progress_loader)
    ProgressBar progressBar;

    private TopRepositoriesComponent topRepositoriesComponent;

    private EndlessRecyclerViewScrollListener scrollListener;

    List<Repositorio> repositorios;

    int quantidadeCallbacks = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TopRepositoriesApplication application = (TopRepositoriesApplication) getApplication();
        topRepositoriesComponent = application.getTopRepositoriesComponent();
        topRepositoriesComponent.inject(this);

        ButterKnife.bind(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                obterRepositorios(page);
            }
        };

        repositorios = new ArrayList<>();

        obterRepositorios(1);

        ListaRepositoriosAdapter adapter = new ListaRepositoriosAdapter(repositorios, this);

        adapter.setOnItemClickListener(position -> irParaTelaDePullRequests(position));

        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(scrollListener);
    }

    private void obterRepositorios(int numeroPagina) {
        Log.d("tag", "buscando a pagina " + numeroPagina);
        progressBar.setVisibility(View.VISIBLE);
        new RepositoriosObserverImpl(this, gitHubService).obterRepositorios(numeroPagina);
    }

    public void inserirNaListaDeRepositorios(Repositorio repositorio) {
        repositorios.add(repositorio);
        int tamanhoDaLista = repositorios.size();
        if (tamanhoDaLista == quantidadeCallbacks) {
            ListaRepositoriosAdapter adapter = getListaRepositoriosAdapter();
            adapter.addItens(repositorios);
            quantidadeCallbacks = 0;
            repositorios.clear();
            progressBar.setVisibility(View.GONE);
        }
    }

    public void irParaTelaDePullRequests(int position) {
        ListaRepositoriosAdapter adapter = getListaRepositoriosAdapter();
        Repositorio repositorio = adapter.getItem(position);
        Intent intentIrParaTelaDePullRequests = new Intent(MainActivity.this, PullRequestsActivity.class);
        intentIrParaTelaDePullRequests.putExtra("repositorio", repositorio);
        startActivity(intentIrParaTelaDePullRequests);
    }

    public ListaRepositoriosAdapter getListaRepositoriosAdapter() {
        ListaRepositoriosAdapter adapter = (ListaRepositoriosAdapter) recyclerView.getAdapter();
        return adapter;
    }

    @Override
    public void inserirNaLista(Repositorio repositorio) {
        inserirNaListaDeRepositorios(repositorio);
    }

    @Override
    public void setarQuantidadeCallbacks(int quantidadeCallbacks) {
        this.quantidadeCallbacks = quantidadeCallbacks;
    }

    @Override
    public void tratarErro(Throwable throwable) {
        Snackbar.make(this.recyclerView, "Não foi possível obter a lista de respositórios.", Snackbar.LENGTH_LONG).show();
        progressBar.setVisibility(View.GONE);
    }
}
