package br.com.concretesolutions.toprepositories.listener;

/**
 * Created by geovan
 */
public interface ItemClickListener {
    void onItemClick(int position);
}
