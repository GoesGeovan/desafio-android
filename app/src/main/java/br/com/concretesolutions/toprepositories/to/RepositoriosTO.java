package br.com.concretesolutions.toprepositories.to;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import br.com.concretesolutions.toprepositories.model.Repositorio;

/**
 * Created by geovan.goes
 */

public class RepositoriosTO implements Serializable {

    @SerializedName("items")
    private List<Repositorio> respositorios;

    @SerializedName("total_count")
    private long total;

    @SerializedName("incomplete_results")
    private boolean incompleteResults;

    public List<Repositorio> getRespositorios() {
        return respositorios;
    }

    public void setRespositorios(List<Repositorio> respositorios) {
        this.respositorios = respositorios;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public boolean isIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }
}
