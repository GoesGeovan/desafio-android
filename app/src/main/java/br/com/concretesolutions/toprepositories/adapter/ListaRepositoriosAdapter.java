package br.com.concretesolutions.toprepositories.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.concretesolutions.toprepositories.R;
import br.com.concretesolutions.toprepositories.holder.ListaRepositoriosViewHolder;
import br.com.concretesolutions.toprepositories.listener.ItemClickListener;
import br.com.concretesolutions.toprepositories.model.Repositorio;

/**
 * Created by geovan
 */

public class ListaRepositoriosAdapter extends RecyclerView.Adapter {

    private List<Repositorio> repositorios = new ArrayList<>();
    private Context context;
    private ItemClickListener itemClickListener;

    @Inject
    Picasso picasso;

    public ListaRepositoriosAdapter(List<Repositorio> repositorios, Context context) {
        if (repositorios != null) {
            this.repositorios.addAll(repositorios);
        }
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemDaLista = LayoutInflater.from(context).inflate(R.layout.item_lista_repositorio, parent, false);
        return new ListaRepositoriosViewHolder(itemDaLista);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ListaRepositoriosViewHolder viewHolder = (ListaRepositoriosViewHolder) holder;

        viewHolder.setOnClickListener(itemClickListener);
        Repositorio repositorio = repositorios.get(position);
        viewHolder.setRepositorio(repositorio);
        viewHolder.nomeRepositorio.setText(repositorio.getName());

        String descricao = repositorio.getDescription();
        viewHolder.descricao.setText(descricao);
        viewHolder.quantidadeStars.setText(String.valueOf(repositorio.getStars()));
        viewHolder.quantidadeForks.setText(String.valueOf(repositorio.getForks()));
        viewHolder.username.setText(repositorio.getUser().getLogin());
        viewHolder.nomeSobrenome.setText(repositorio.getUser().getName());
        picasso.with(context).load(repositorio.getUser().getUrlAvatar()).into(viewHolder.avatarUsuario);
    }

    @Override
    public int getItemCount() {
        return repositorios.size();
    }

    public void addItens(List<Repositorio> repositorios) {
        this.repositorios.addAll(repositorios);
        notifyDataSetChanged();
    }

    public void addItem(Repositorio repositorio) {
        this.repositorios.add(repositorio);
        notifyDataSetChanged();
    }

    public Repositorio getItem(int position) {
        return this.repositorios.get(position);
    }


    public void setOnItemClickListener(ItemClickListener listener) {
        this.itemClickListener = listener;
    }

    public List<Repositorio> getRepositorios() {
        return this.repositorios;
    }
}
