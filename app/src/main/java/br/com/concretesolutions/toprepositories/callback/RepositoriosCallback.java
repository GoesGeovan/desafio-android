package br.com.concretesolutions.toprepositories.callback;

import br.com.concretesolutions.toprepositories.model.Repositorio;

/**
 * Created by geovan
 */

public interface RepositoriosCallback {
    void inserirNaLista(Repositorio repositorio);
    void setarQuantidadeCallbacks(int quantidadeCallbacks);
    void tratarErro(Throwable throwable);

}
