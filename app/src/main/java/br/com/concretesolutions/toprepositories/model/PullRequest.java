package br.com.concretesolutions.toprepositories.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by geovan.goes
 */

public class PullRequest implements Serializable {

    private long id;

    private String title;

    private String state;

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("created_at")
    private String dataCriacao;

    private String body;

    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public String getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(String dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
