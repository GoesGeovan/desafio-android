package br.com.concretesolutions.toprepositories.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by geovan.goes
 */

public class User implements Serializable {

    private long id;

    private String login;

    @SerializedName("avatar_url")
    private String urlAvatar;

    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getUrlAvatar() {
        return urlAvatar;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
