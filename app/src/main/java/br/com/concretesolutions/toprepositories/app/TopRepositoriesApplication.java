package br.com.concretesolutions.toprepositories.app;

import android.app.Application;

import br.com.concretesolutions.toprepositories.component.DaggerTopRepositoriesComponent;
import br.com.concretesolutions.toprepositories.component.TopRepositoriesComponent;
import br.com.concretesolutions.toprepositories.module.TopRepositoriesModule;

/**
 * Created by geovan
 */

public class TopRepositoriesApplication extends Application {

    private TopRepositoriesComponent topRepositoriesComponent;

    @Override
    public void onCreate() {
        topRepositoriesComponent = DaggerTopRepositoriesComponent
                .builder()
                .topRepositoriesModule(new TopRepositoriesModule(this))
                .build();
    }

    public TopRepositoriesComponent getTopRepositoriesComponent(){
        return topRepositoriesComponent;
    }

}
