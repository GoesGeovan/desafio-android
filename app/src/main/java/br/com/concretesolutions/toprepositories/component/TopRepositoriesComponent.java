package br.com.concretesolutions.toprepositories.component;

import br.com.concretesolutions.toprepositories.activity.MainActivity;
import br.com.concretesolutions.toprepositories.activity.PullRequestsActivity;
import br.com.concretesolutions.toprepositories.module.TopRepositoriesModule;
import dagger.Component;

/**
 * Created by geovan.goes
 */

@Component(modules = TopRepositoriesModule.class)
public interface TopRepositoriesComponent {

    void inject (MainActivity mainActivity);
    void inject (PullRequestsActivity pullRequestsActivity);
}