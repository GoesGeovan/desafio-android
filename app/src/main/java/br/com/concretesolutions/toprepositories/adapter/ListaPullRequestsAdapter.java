package br.com.concretesolutions.toprepositories.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import br.com.concretesolutions.toprepositories.R;
import br.com.concretesolutions.toprepositories.holder.ListaPullRequestsViewHolder;
import br.com.concretesolutions.toprepositories.listener.ItemClickListener;
import br.com.concretesolutions.toprepositories.model.PullRequest;

/**
 * Created by geovan
 */

public class ListaPullRequestsAdapter extends RecyclerView.Adapter {

    private Context context;

    private List<PullRequest> pullRequests = new ArrayList<>();

    private ItemClickListener itemClickListener;

    @Inject
    Picasso picasso;

    public ListaPullRequestsAdapter(Context context, List<PullRequest> pullRequests) {
        if (pullRequests != null) {
            this.pullRequests.addAll(pullRequests);
        }
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemDaLista = LayoutInflater.from(context).inflate(R.layout.item_lista_pull_request, parent, false);
        return new ListaPullRequestsViewHolder(itemDaLista);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PullRequest pullRequest = this.pullRequests.get(position);
        ListaPullRequestsViewHolder listaPullRequestsViewHolder = (ListaPullRequestsViewHolder) holder;

        listaPullRequestsViewHolder.setOnClickListener(this.itemClickListener);

        listaPullRequestsViewHolder.tituloPullRequest.setText(pullRequest.getTitle());

        String dataCriacao = pullRequest.getDataCriacao();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = null;
        try {
            parse = simpleDateFormat.parse(dataCriacao);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dataCriacao = parse != null ? simpleDateFormat.format(parse) : "";

        listaPullRequestsViewHolder.dataPullRequest.setText(dataCriacao);

        String body = pullRequest.getBody();

        listaPullRequestsViewHolder.descricaoPullRequest.setText(body);

        listaPullRequestsViewHolder.nomeSobrenome.setText(pullRequest.getUser().getName());

        listaPullRequestsViewHolder.username.setText(pullRequest.getUser().getLogin());
        picasso.with(context).load(pullRequest.getUser().getUrlAvatar()).into(listaPullRequestsViewHolder.avatarUsuario);
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public void setOnItemClickListener(ItemClickListener listener) {
        this.itemClickListener = listener;
    }

    public void addItems(List<PullRequest> pullRequests) {
        this.pullRequests.addAll(pullRequests);
        notifyDataSetChanged();
    }

    public void addItem(PullRequest pullRequest) {
        this.pullRequests.add(pullRequest);
        notifyDataSetChanged();
    }

    public PullRequest geItem(int position) {
        return this.pullRequests.get(position);
    }
}
